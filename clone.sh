#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Configuration
# -----------------------------------------------------------------------------

FROM="skeleton"
FROM_MODULE="Skeleton"
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)

# -----------------------------------------------------------------------------
# Generación del secret key
# -----------------------------------------------------------------------------
case "$OSTYPE" in
  darwin*)  NEW_UUID=$(cat /dev/urandom | env LC_CTYPE=C tr -dc a-zA-Z0-9 | head -c 64; echo) ;;
  *)        $NEW_UUID ;;
esac

# -----------------------------------------------------------------------------
# Validación
# -----------------------------------------------------------------------------

if [[ -z $(echo "$1" | grep "^[A-Z]") ]] ; then
  echo 'Debe especificar el nombre de su proyecto en PascalCase como primer argumento (por ejemplo, FooBar).'
  exit 0
fi

TO_MODULE="$1"
TO=$(echo $TO_MODULE | sed 's/\(.\)\([A-Z]\{1,\}\)/\1_\2/g' | tr '[:upper:]' '[:lower:]')
UNDER=$(echo $TO_MODULE | tr '[:upper:]' '[:lower:]')

# -----------------------------------------------------------------------------
# Funciones auxiliares
# -----------------------------------------------------------------------------

header() {
  printf "\033[0;33m▶ $1\033[0m\n"
}

success() {
  printf "\033[0;32m▶ $1\033[0m\n"
}

run() {
  echo ${@}
  eval "${@}"
}

# -----------------------------------------------------------------------------
# Execución
# -----------------------------------------------------------------------------

header "Configuración"
echo "${FROM} → ${TO}"
echo "${FROM_MODULE} → ${TO_MODULE}"
echo ""

header "Renombrando el proyecto"
run git ls-files -z | xargs -0 perl -p -i -e "s/$FROM/$TO/g; s/$FROM_MODULE/$TO_MODULE/g; s/secret_key_base_64/$NEW_UUID/g;"
run mv "lib/${FROM}" "lib/$TO"
run mv "lib/${FROM}.ex" "lib/${TO}.ex"
run mv "lib/${FROM}_web" "lib/${TO}_web"
run mv "lib/${FROM}_web.ex" "lib/${TO}_web.ex"
run mv "test/${FROM}_web" "test/${TO}_web"

header "Eliminando git"
run rm -rfv .git
success "Hecho!\n"

header "Creando nuevo git"
run git init .
success "Hecho!\n"

header "Corrigiendo okteto.yml"
run sed -i "'s/name: $TO*/name: $UNDER/g'" ./okteto.yml
success "Hecho!\n"

header "Quitando scripts"
run rm ./clone.sh ./install
success "Hecho!\n"
