export ERL_AFLAGS="-kernel shell_history enabled"
export APP_NAME=skeleton
export HOST="https://canary-backofficedigital.totalcheck.cl/accounts/login?provider=alberto&redirect=https://canaryy-backofficedigital.totalcheck.cl"
MIX_ENV=dev iex --cookie node --name $APP_NAME@`getent hosts ${1:-$HOSTNAME} | awk '{print $1}'` -S mix phx.server

