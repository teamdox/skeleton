
FROM beardedeagle/alpine-phoenix-builder:1.11.3 as builder


ENV appdir /opt/skeleton
ENV MIX_ENV=prod \
REPLACE_OS_VARS=true \
APP_NAME=skeleton \
RELEASE_NAME=skeleton0
WORKDIR ${appdir}
COPY . .
RUN apk update
RUN apk add --no-cache \
	curl 
RUN mix local.rebar --force
RUN mix local.hex --force 
RUN curl -O "https://s3.amazonaws.com/rebar3/rebar3" && chmod +x rebar3 && ./rebar3 local install
ENV PATH "/root/.cache/rebar3/bin:$PATH"
RUN rebar3 --version
RUN apk --no-cache --update add build-base
RUN apk add --update npm
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN mix clean \
  && mix deps.get  \
  && mix compile \
  && cd assets \
  && npm install \
  && node node_modules/webpack/bin/webpack.js --mode production \
  && cd ${appdir} \
  && mix phx.digest \
  && mix release ${RELEASE_NAME} 

FROM alpine:3.12.1
EXPOSE 4000
ENV appver=0.1.0 \
    APP_NAME=skeleton \
    RELEASE_NAME=skeleton0 \
    MIX_ENV=prod

WORKDIR /opt/skeleton
COPY --from=builder /opt/${APP_NAME}/_build/${MIX_ENV}/rel/${RELEASE_NAME} ./
RUN apk add --no-cache bash libressl 
CMD ["bin/skeleton0", "start"]


