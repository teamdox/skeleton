defmodule Skeleton.Net do
  require Monad.Error, as: Error
  @etcd_key "/hosts"
  @deps ["nodeservice", "userservice", "searchservice"]
  def pg2(a) do
    case :pg2.get_closest_pid(a) do
      {:error, a} -> Error.fail(a)
      a -> Error.return(a)
    end
  end

  defp etcdc_nodes(%{:node => %{:nodes => nodes}}), do: Error.return(nodes)
  defp etcdc_nodes(a), do: Error.fail("etcd internal error #{inspect(a)}")

  def world do
    Error.m do
      n <- :etcdc.get(@etcd_key)
      configs <- n |> etcdc_nodes

      Error.return(
        configs
        |> Enum.filter(&(&1.key |> String.contains?(@deps)))
        |> Enum.map(
          &(&1
            |> Map.get(:value)
            |> String.to_atom()
            |> Node.connect())
        )
      )
    end
  end
end
