defmodule SkeletonWeb.Plugs.Ticket do
  import Plug.Conn
  alias Skeleton.Alberto

  def init(opts), do: opts
  def call(conn, _opts) do
    case {:ticketinfo_m, conn.req_cookies["ticket"]} |> AlbertoLib.S.call(:userservice) do
      {:ok, _} ->
        conn
        |> put_session(:ticket, conn.req_cookies["ticket"])
        |> put_session(:username, conn.req_cookies["username"])
        |> put_session(:host, "https://" <> hostname(conn))
      _ ->
        conn
        |> put_resp_cookie("ticket", "", [http_only: false, max_age: -1, domain: domain(conn)])
        |> put_resp_cookie("username", "", [http_only: false, max_age: -1, domain: domain(conn)])
        |> put_resp_cookie("_skeleton_key", "", [http_only: false, max_age: -1, domain: hostname(conn)])
        |> put_resp_header("location", System.get_env("MIX_ENV") |> redirect_to(conn))
        |> resp(:found, "")
        |> halt()
    end
  end

  defp redirect_to("dev", conn), do: System.get_env("HOST") <> conn.request_path
  defp redirect_to(_, conn), do: "https://" <> hostname(conn) <> "/accounts/login?provider=alberto&redirect=" <> "https://" <> hostname(conn) <> conn.request_path
  defp hostname(conn), do: Enum.into(conn.req_headers, %{}) |> Map.get("host")
  defp domain(conn), do: Regex.run(~r/\.\w+[\-]?\w+\.\w+$/, hostname(conn)) |> List.first()
end
