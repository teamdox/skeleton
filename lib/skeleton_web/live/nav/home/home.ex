defmodule SkeletonWeb.Nav.Home do
  use SkeletonWeb, :live_view
  alias Skeleton.{Alberto, Utils}
  alias PhoenixComponents.Table.{Simple, Column}


  data page_title, :string, default: "Mis expedientes"
  data inbox, :string, default: "inicio"

  @impl true
  def mount(_params, session, socket) do
    {:ok,
     socket
     |> assign(:opts, session["opts"])
     |> assign(:user, session["username"] |> Alberto.user)
     |> assign(:ticket, session["ticket"])
    }
  end

  @impl true
  def handle_params(params, _url, socket) do
    page = params["page"] || 1
    perpage = params["per_page"] || 10

    data =
      Alberto.search_m(page, perpage, "solicitud_servicio")
      |> Alberto.clean_monad_records()

    {:noreply,
      socket
      |> assign(:query_params, params)
      |> assign(data)
      |> Simple.handle_params(params)
    }
  end
end
