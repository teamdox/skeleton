defmodule SkeletonWeb.Nav.Home.Details do
  use SkeletonWeb, :live_view
  alias PhoenixComponents.{
    Preview,
    Preview.Wrapper,
    NodeDetail,
    NodeDetail.Detail,
    NodeDetail.Detail.Actions,
    Node
  }

  alias Skeleton.Alberto
  require Monad.Error, as: Error


  data page_title, :string, default: "Detalles de solicitudes"
  data inbox, :string, default: "inicio"

  @impl true
  def mount(_params, session, socket) do
    {:ok,
     socket
     |> assign(:opts, session["opts"])
     |> assign(:user, session["username"] |> Alberto.user)
     |> assign(:ticket, session["ticket"])
     |> assign(:url, session["host"])
    }
  end

  @impl true
  def handle_event("refreshdoc", params, socket) do
    {:noreply, socket |> Phoenix.LiveView.push_event("refreshdoc", params)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, socket |> assign(:node, params |> info_m) }
  end

  # Funcion pura para obtener nro_solicitud y uniqueid
  defp info_m(params) do
    Error.m do
      uniqueid <- uniqueid_params(params)
      node <- Alberto.node_m(uniqueid)
      children <- Alberto.children(uniqueid)
      selected <- List.first(children) |> Alberto.uniqueid |> Error.return()
      {:ok, %{node: node, selected: selected, children: children}}
    end
  end

  # Funcion pura para obtener uniqueid
  defp uniqueid_params(%{"unique_id" => uniqueid}), do: Error.return(uniqueid)
  defp uniqueid_params(_), do: Error.fail(%{})

  defp config, do:
    %{
      "translate" => %{
        "inserted_at" => "Fecha creación",
        "creator" => "Nombre del solicitante",
        "nro_solicitud" => "Número de solicitud",
        "adquirientes" => "Datos del cliente",
        "cond_pago" => "Forma de pago",
        "direccion_completa" => "Dirección completa",
        "fecha_personeria" => "Fecha escritura pública",
        "ciudad_personeria" => "Ciudad personería",
        "notario_personeria" => "Notario personería",
        "datos_del_vehiculo" => "Datos del Vehículo",
        "ocupacion" => "Ocupación",
        "numero_motor" => "Número motor",
        "tipo_vehiculo" => "Tipo vehículo",
        "anual" => "Año",
        "chassis" => "Chasis",
        "telefono" => "Teléfono"
      },
      "key_persons" => [["data", "datos_del_cliente"]],
      "key_new" => %{
        "put" => ["data"],
        "fields" => %{
          "precio" => %{
            "parent" => ["data", "documento"],
            "children" => ["monto_total", "monto_exento"]
          },
          "datos_del_vehiculo" => %{
            "parent" => ["data", "vehiculo"],
            "children" => ["patente", "anual", "color", "marca", "modelo", "numero_motor", "tipo_vehiculo", "chassis"]
          },
          "datos_del_cliente" => %{
            "parent" => ["data", "adquirientes"],
            "children" => ["nombre_completo", "rut", "giro", "direccion_completa", "comuna", "ciudad"]
          },
          "solicitud" => [["data", "tipo_servicio"], ["data", "nro_solicitud"], ["inserted_at"], ["creator"]]
        }
      },
      "take_group" => [[["data"], "solicitud", "precio", "datos_del_cliente", "datos_del_vehiculo"]],
      "sort_group" => ["solicitud", "datos_del_vehiculo", "datos_del_cliente", "precio"],
      "delete_by_keys" => ["nombre","primer_apellido"]
    }
end
