defmodule SkeletonWeb.Alerts do
  @moduledoc false

  use SkeletonWeb, :live_view_without_layout
  import Skeleton.Utils

  @impl true
  def mount(_params, session, socket) do
    subscribe_to_alarms_topic(session)

    socket =
      socket
      |> assign(:content, nil)
      |> assign(:type, nil)
      |> assign(:show, false)

    {:ok, socket}
  end

  @impl true
  def handle_info({:warn, message}, socket) do
    send(self(), {:schedule_clear_message, :info})
    {:noreply, assign(socket, content: message, type: :warn, show: true)}
  end

  @impl true
  def handle_info({:info, message}, socket) do
    send(self(), {:schedule_clear_message, :info})
    {:noreply, assign(socket, content: message, type: :info, show: true)}
  end

  @impl true
  def handle_info({:success, message}, socket) do
    send(self(), {:schedule_clear_message, :success})
    {:noreply, assign(socket, content: message, type: :success, show: true)}
  end

  @impl true
  def handle_info({:error, message}, socket) do
    send(self(), {:schedule_clear_message, :error})
    {:noreply, assign(socket, content: message, type: :error, show: true)}
  end

  @impl true
  def handle_info({:schedule_clear_message, _}, socket) do
    :timer.sleep(3000)
    {:noreply, assign(socket, content: nil, show: false)}
  end

  defp subscribe_to_alarms_topic(session) do
    topic = alerts_topic_id(session)
    Phoenix.PubSub.subscribe(Skeleton.PubSub, topic, link: true)
  end

  defp bgcolor(:warn, num), do: "bg-yellow-#{num}"
  defp bgcolor(:success, num), do: "bg-green-#{num}"
  defp bgcolor(:error, num), do: "bg-red-#{num}"
  defp bgcolor(_, num), do: "bg-blue-#{num}"

end
