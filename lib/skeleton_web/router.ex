defmodule SkeletonWeb.Router do
  use SkeletonWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug SkeletonWeb.Plugs.SessionId
    plug :put_root_layout, {SkeletonWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SkeletonWeb.Plugs.Ticket
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SkeletonWeb do
    pipe_through :browser

    live "/skeleton", Nav.Home
    live "/skeleton/inicio", Nav.Home
    live "/skeleton/inicio/detalles", Nav.Home.Details
  end

  # Funcion auxiliar
  def pathname(path), do: "/skeleton/#{path}"

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/skeleton/dashboard", metrics: SkeletonWeb.Telemetry, live_socket_path: "/skeleton/live"
    end
  end
end
