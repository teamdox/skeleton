defmodule SkeletonWeb.LayoutView do
  use SkeletonWeb, :view
  alias Skeleton.{Alberto, Utils}
  alias SkeletonWeb.Components.SheetMenu
  alias PhoenixComponents.{
    MiddleMenubar,
    MainLayout,
    MainLayout.Navbar,
    MainLayout.Header
  }

  def render("live", assigns) do
    ~F"""
      <MainLayout class="px-6">
        <Header class="px-6">
          <Header.Left>
            <PhoenixComponents.RoutePatch route={SkeletonWeb.Nav.Home} class="inline-flex py-2 px-2 mr-4">
              <PhoenixComponents.Svg name="backofficedigital" class="h-9" />
            </PhoenixComponents.RoutePatch>
          </Header.Left>
          <Header.Right>
            <x-apps data-first="solicitudservicios" data-prefix="False" data-applist={Alberto.apps(@user)}></x-apps>
            <PhoenixComponents.Profile {=@ticket} redirect={Routes.live_path(@socket, SkeletonWeb.Nav.Home)} id="profile" />
          </Header.Right>
        </Header>
        <Navbar class="px-6 bg-white shadow">
          <MiddleMenubar>
            <MiddleMenubar.Left>
            <PhoenixComponents.RoutePatch label="Inicio" route={SkeletonWeb.Nav.Home} class={"inline-flex py-2 px-2 mr-4 text-gray-400 hover:text-gray-600 border-b-4 border-white transition duration-300 hover:border-primary-300", "text-gray-600 border-primary-300": (String.contains? @inbox, ["inicio"])} />
            </MiddleMenubar.Left>
            <MiddleMenubar.Right>
              <PhoenixComponents.Svg name="portfolio" class="mr-2" />
              <PhoenixComponents.Svg name="notification" />
            </MiddleMenubar.Right>
          </MiddleMenubar>
        </Navbar>
        {@inner_content}
      </MainLayout>
    """
  end

  def render(_, assigns) do
    ~F"""
      <!DOCTYPE html>
      <html lang="en" class="bg-html">
        <head>
          <meta charset="utf-8"/>
          <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          {csrf_meta_tag()}
          {live_title_tag assigns[:page_title] || "Skeleton", suffix: " · Phoenix Framework"}
          <link phx-track-static rel="stylesheet" href={Routes.static_path(@conn, "/css/app.css")}>
          <script defer phx-track-static type="text/javascript" src={Routes.static_path(@conn, "/js/app.js")}></script>
          <script src="/microfront/apps/apps.js" />
        </head>
        <body>
          {@inner_content}
          {live_render @conn, SkeletonWeb.Alerts}
        </body>
      </html>
    """
  end
end
