defmodule SkeletonWeb.Components.CustomNode do
  use Surface.Component
  require Monad.Error, as: Error
  alias Skeleton.{Utils}
  alias PhoenixComponents.Node

  prop node, :any, required: true
  prop config, :any, default: %{}

  slot default

  def from_string(param, config), do:
    param
    |> Node.translet(config["translate"])
    |> Node.apply_by_date

  def from_parent(param, config) do
    param
    |> Node.new_map(config["key_new"])
    |> Node.updated_data(config["key_persons"], {"tipo_persona", "rut"}, &Utils.rut_string/1 )
    |> Node.take_group(config["take_group"])
    |> Node.sort(config["sort_group"])
  end

  def from_child(param, config), do: Node.delete(param, config["delete_by_keys"])
end
