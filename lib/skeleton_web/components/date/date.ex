defmodule SkeletonWeb.Components.Date do
  use Surface.Component

  prop date, :string, required: true
end
