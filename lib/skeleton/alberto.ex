defmodule Skeleton.Alberto do
  require Monad.Error, as: Error
  alias AlbertoLib.{S}

  # Obenter informacion del usuario
  def user_m(name), do: {:bypath_m, "/users/#{name}"} |> S.call("node")
  def user(name), do: user_m(name) |> get_value_monad

  # Obtiene los hijos secundarios de un nodo
  def children(unique_id),
    do: {:nodechild, unique_id, :secondary} |> S.call("node") |> Error.return()

  # Obtiene la info de un nodo alberto
  def node_m(unique_id), do: {:node_m, unique_id} |> S.call("node")

  # Obtiene contenido del documento en alberto
  def nodecontent_m(unique_id) do
    Error.m do
      pid <- Skeleton.Net.pg2("node")
      pid |> nodecontent(unique_id)
    end
  end

  # Funcion auxiliar para eliminar ticket
  defp nodecontent(pid, unique_id) do
    try do
      GenServer.call(pid, {:nodecontent, unique_id}) |> Error.return()
    catch
      :exit, code -> Error.fail("Exited #{inspect code}")
      :throw, value -> Error.fail("Throw #{inspect value}")
      what, value -> Error.fail("Caught #{inspect what} with #{inspect value}")
    end
  end

  # Actualiza nodo alberto
  def merge_m(data, unique_id), do: {:datamerge_m, unique_id, data} |> S.call("node")

  def merge_nodes(nodes, data), do:
    Error.return(nodes |> Enum.map(&(merge_m(data, uniqueid(&1)) |> get_value_monad())))

  # Asociando nodos
  def addsecondary(child, parent), do:
    {:addsecundaryparent, child, parent} |> S.call("node")

  #Asocia padre con hijos
  def addchildren(children, parent), do:
    Error.return(children |> Enum.map(&(addsecondary(uniqueid(&1), uniqueid(parent)) |> get_value_monad())))

  # Desasociando nodos
  def removesecondary(child, parent), do:
    {:removesecundaryparent, child, parent} |> S.call("node")

  # Crear solicitud
  def createdocumental(data, username, tenant, type) do
    Error.m do
      pid <- Skeleton.Net.pg2("node")
      pid |> call({:package, %{"data" => %{"username" => username}}, data, tenant, type})
    end
  end

  # Para subir documentos
  def upload(content, data, filename, desc, type, user) do
    Error.m do
      parent <- tenant(user) |> parent_m(type)
      pid <- Skeleton.Net.pg2("node")
      pid |> call_m({:upload, %{
        file: {filename, content},
        type: type,
        title: filename,
        description: desc,
        parent: parent,
        user: user
        }, data})
    end
  end

  # Obtiene nodo padre
  def parent_m(tenant, type) do
    Error.m do
      pid <- Skeleton.Net.pg2("node")
      pid |> call_m({:package, tenant, type})
    end
  end

  # Busqueda en alberto
  def get_exp_by_status(from, size, type, status, sort \\ "inserted_at:desc") do
    elastic_list([term("estado_solicitud", status) ])
    |> search_m_filter(from, size, type, sort)
  end

  def get_exp_by_id(from, size, type, nrosolocitud, sort \\ "inserted_at:desc") do
    elastic_list([term("nro_solicitud", nrosolocitud) ])
    |> search_m_filter(from, size, type, sort)
  end

  def search_m(from, size, type, sort \\ "inserted_at:desc") do
    base_filter()
    |> search_m_filter(from, size, type, sort)
  end

  def search_m_filter(filter, from, size, type, sort \\ "inserted_at:desc") do
    Error.m do
      search <- filter |> payload_elastic(from, size, type, sort)
      search |> apply_search()
    end
  end

  # Busquedas en el índice de elastic
  def apply_search(search) do
    with hits <- search |> hits(),
    total_records <- search |> total(),
    records <- hits |> Enum.map(&source/1) do
      %{records: records, total_records: total_records} |> Error.return()
    end
  end

  # Llamada a funcion remota de alberto para busquedas
  defp payload_elastic(filter, from, size, type, sort) do
    {:complex_system_match, type, "estatico", filter, to_from(from, size), to_integer(size), sort}
    |> S.call(:searchservice)
    |> Error.return()
  end

  # Filro basico
  defp base_filter() do
    %{}
    |> Map.merge(filter_base())
    |> bool()
  end

  # Filro basico personalizado
  defp elastic_list(list) do
    %{}
    |> Map.merge(must(list))
    |> Map.merge(filter_base())
    |> bool()
  end

  # Eliminar ticket
  def logout(ticket) do
    Error.m do
      pid <- Skeleton.Net.pg2(:admticket_srv)
      pid |> call_m({:killticket, ticket}) |> process_logout()
    end
  end

  def process_logout(:ok), do: Error.return("Ticket Removed")
  def process_logout({:error, e}), do: Error.fail(e)

  def getdata({:ok, %{:data => data}}), do: data
  def getdata({:error, _}), do: %{}

  def data_m({:ok, %{"data" => data}}), do: Error.return(data)
  def data_m({:error, _}), do: Error.fail(:empty)

  # Obtiene los registros
  def records(%{records: records}), do: records

  def listnode([{:ok, data} | _]), do: data
  def listnode(_), do: []

    # Extraer metadato de un monad
  def get_value_monad({:ok, r}), do: r
  def get_value_monad({:error, e}), do: e

  # Obtiene el tenant del usuario
  def tenant(%{"data" => %{"primary_tenant" => tenant}}), do: tenant
  def tenant(%{data: %{"primary_tenant" => tenant}}), do: tenant

  # Obtiene el tenant del usuario
  def uniqueid(%{unique_id: id}), do: id
  def uniqueid({:ok, %{"unique_id" => id}}), do: id
  def uniqueid(%{"unique_id" => id}), do: id
  def uniqueid(_), do: :empty

  # Obtiene nombre
  def name({:ok, %{name: name}}), do: Error.return(name)
  def name(_), do: Error.fail("")

  # Obtiene las aplicaciones del usuario
  def apps(%{"data" => %{"applications" => apps}}), do: Macro.to_string(apps)
  def apps(%{data: %{"applications" => apps}}), do: Macro.to_string(apps)
  def apps(_), do: Macro.to_string([])

  # Obtiene username
  def username(%{"name" => name}), do: name
  def username(%{name: name}), do: name

  # Obtiene nombre usuario
  defp fullname_data({:ok, %{:data => %{"nombres" => n, "paterno" => p, "materno" => m}}}, _), do: "#{n} #{p} #{m}"
  defp fullname_data(_, name), do: name

  # Obtiene nombre completo usuario
  def fullname(name) do
    Error.m do
      name |> user_m
    end |> fullname_data(name)
  end

  # Extrae metadatos de una funcion monad
  def clean_monad_records({:ok, data}), do: data
  def clean_monad_records(_), do: %{records: [], total_records: 0}

  # Extrae records
  def get_records(%{:records => records}), do: Error.return(records)

  # Calcula a partir de donde se obtengran los registros
  defp to_from(page, perpage), do: to_integer(perpage) * (to_integer(page) - 1) |> verify

  # Corregir cuando es zero o menor
  defp verify(num) when num < 1, do: 0
  defp verify(num), do: num

  # Convierte a entero el parametro
  defp to_integer(num) when is_integer(num), do: num
  defp to_integer(num) when is_binary(num), do: String.to_integer(num)
  defp to_integer(_), do: 1

  def call_m(pid, service) do
    try do
      GenServer.call(pid, service)
    catch
      :exit, code -> Error.fail("Exited #{inspect code}")
      :throw, value -> Error.fail("Throw #{inspect value}")
      what, value -> Error.fail("Caught #{inspect what} with #{inspect value}")
    end
  end

  def call(pid, service) do
    try do
      GenServer.call(pid, service) |> Error.return()
    catch
      :exit, code -> Error.fail("Exited #{inspect code}")
      :throw, value -> Error.fail("Throw #{inspect value}")
      what, value -> Error.fail("Caught #{inspect what} with #{inspect value}")
    end
  end

  # Elasticsearch
  def terms(field, value), do: %{"terms" => %{field => value}}
  def term(field, value), do: %{"term" => %{field => value}}
  def query_string(field, value), do: %{"query_string" => %{"default_field" => field, "query" => value}}
  def must_not(a), do: %{"must_not" => a}
  def must(a), do: %{"must" => a}
  def should(a), do: %{"should" => a}
  def min_should_match(number), do: %{"minimum_should_match" => number}
  def bool(a), do: %{"bool" => a}
  def filter(a), do: %{"filter" => a}
  def prefix(field, value), do: %{"prefix" => %{field => value}}
  def range(field, min, max), do: %{"range" => %{field => %{"gte" => min, "lte" => max}}}
  def match(field, value), do: %{"match" => %{field => value}}
  def filter_base(), do: term("deleted", true) |> must_not()
  def total(%{"hits" => %{"total" => total}}), do: total
  def hits(%{"hits" => %{"hits" => hits}}), do: hits
  def source(%{"_source" => source}), do: source

  # Aqui se aplica un reduce a las condicionales para darle el formato adecuado
  # a la consulta que se le pasa a elastic
  def union_condition(condicionales, filtro) do
    condicionales
    |> Enum.reduce(filtro, fn x, acc ->
        key = x |> Map.keys() |> List.first()
        value = x |> Map.get(key)
        filter_update(acc, key, value)
      end)
  end

  # Esta funcion busca en un mapa si hay alguna llave con el valor de Key,
  # si no la encuentra, inserta al mapa la llave(Key) y el valor(value)
  # si la encuentra, pues se crea una lista con el valor que existia(existing) + el nuevo(value)
  def filter_update(results, key, value ) do
    Map.update(
      results, # busca en el mapa que existe de los filtros
      key, # por una entrada con esta llave
        value , # si no encuentra la entrada, agrega la llave con el valor
      fn existing -> # si lo encuentra actualiza el valor correspondiente a esa llave con la funcion
      List.flatten([ value ] ++ [ existing ])
      end)
  end
end
