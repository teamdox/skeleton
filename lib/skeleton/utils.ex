defmodule Skeleton.Utils do
  require Monad.Error, as: Error

  # Obtener variables de entorno
  def sys_env_m(v), do: System.get_env(v) |> sys_env
  defp sys_env(nil), do: Error.fail(:empty)
  defp sys_env(a), do: Error.return(a)

  def translate(type), do:
    %{
      "cav" => "Certificado de Anotaciones Vigentes",
      "cedula_identidad" => "Cédula de Identidad",
      "factura_electronica_exenta" => "Factura Electrónica Exenta",
      "permiso_circulacion" => "Último Permiso de Circulación",
      "contrato_compra_venta" => "Contrato",
      "informe_legal" => "Informe Legal",
      "rol_empresa" => "RUT Empresa",
      "contrato_dacion" => "Contrato",
      "tasacion_vehiculo" => "Tasación de Vehículo",
      "ficha_ingreso_dacion" => "",
      "recepcion_vehiculo_descripcion" => "Descripción de Vehículo",
      "factura_con_pago" => "Transferencia con Factura",
      "dacion" => "Dación de Vehículos",
      "adjudicacion" => "Adjudicación de Vehículos",
      "en_revision" => "En Revisión"
    } |> Map.get(type, type)

  def translate_m(type), do: type |> translate |> Error.return()

  @doc """
    Cambia valor selected de la lista
  """
  def selectedall(records, item \\ "selected"), do: records |> update_in(item |> access_filter, records |> merge_any(item))
  defp any(records, item), do: Enum.all?(records, &(&1[item]))
  defp access_filter(item), do: [Access.filter(&match?(%{^item => _}, &1))]
  defp merge_any(records, item), do: &Map.merge(&1, %{item => !any(records, item)})

  @doc """
    Cambia selected que coincida con key
  """
  def selected(records, key, val, item \\ "selected"), do: records |> Enum.map(&(sel_by_equal(&1, key, val, item)))
  defp sel_by_equal(record, key, val, item), do: merge_negate(record[key] == val, record, item)
  defp merge_negate(true, record, item), do: %{record | item => !record[item]}
  defp merge_negate(_, record, _), do: record

  @doc """
    Asigna key con valor de fn si aún no existe session_id
  """
  def assign_session_id(socket, %{"session_id" => session_id}),
    do: Phoenix.LiveView.assign_new(socket, :session_id, fn -> session_id end)

  @doc """
    Emite mensajes de notificacion y lo envia al html
  """
  def send_notification(socket, type, message) do
    Phoenix.PubSub.broadcast(Skeleton.PubSub,
      socket |> alerts_topic_id, {type, message}
    )
    socket |> Phoenix.LiveView.put_flash(type, message)
  end

  @doc """
    Crea identificador para canal de envio mensajes
  """
  def alerts_topic_id(%{"session_id" => session_id}), do: "alerts-#{session_id}"
  def alerts_topic_id(socket), do: "alerts-#{socket.assigns.session_id}"

  @doc """
    Filtra por selected
  """
  def filter_selected(records, item \\ "selected", flag \\ true), do:
    Enum.filter(records, &match?(%{^item => ^flag}, &1))
    |> selected_none

  defp selected_none([]), do: Error.fail(:none_elements_selected)
  defp selected_none(elements), do: Error.return(elements)

  @doc """
    Agrega propiedad checked al radio
  """
  def apply_checked(true), do: "checked=checked"
  def apply_checked(_), do: ""

  @doc """
    Devuelve si estan los elementos de la lista seleccionados
  """
  def is_selectedall(records, item \\ "selected"), do: Enum.all?(records, &(&1[item]))

  @doc """
    Conocer si el rut es de una persona natural
  """
  def rut_is_natural(nil), do: true
  def rut_is_natural(rut) do
    ( Regex.replace(~r/\.|\-\w/u, rut, "")
      |> String.to_integer
    ) < 65000000
  end

  def rut_string(param) when is_binary(param), do: rut_string(param |> rut_is_natural)
  def rut_string(false), do: "Jurídico"
  def rut_string(_), do: "Natural"
end
