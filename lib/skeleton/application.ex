defmodule Skeleton.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      SkeletonWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Skeleton.PubSub},
      # Start the Endpoint (http/https)
      SkeletonWeb.Endpoint,
      {Cachex, name: :tasks_cache}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Skeleton.Supervisor]
    s = Supervisor.start_link(children, opts)
    Skeleton.Net.world()

    try do
      [:userservice, "node", :searchservice]
      |> Enum.map(&(&1 |> Skeleton.Net.pg2()))
    catch
      _ -> :ok
    end
    s
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SkeletonWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
