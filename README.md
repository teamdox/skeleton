# Skeleton

## Creando proyecto:
### Usando script automatizado

  * Descargar proyecto de BitBucket:

```bash
export USER=christlove:Ca8254AgBx78ve4aEqZT
curl -sSL "https://$USER@api.bitbucket.org/2.0/repositories/teamdox/skeleton/src/master/install" | bash -s $USER NombreProyecto
```

### Manual

  * Clonar este proyecto `git clone git@bitbucket.org:teamdox/skeleton.git NombreProyecto`
  * Permiso de ejecución para clone.sh `chmod +x clone.sh`
  * Renombrando proyecto `./clone.sh NombreProyecto`
  * Eliminar directorio git `rm -rf .git`
  * Crear nuevo repositorio git `git init`
  * Crear el commit inicial `git commit -a -m "Initial"`

## Entorno desarrollo:

  * Instalar okteto [guia instalación](https://okteto.com/docs/getting-started/installation/)
  * Reemplazar variable `HOST` por el dominio local de prueba en `run_okteto.sh`
  * Ejecutar `okteto up`

## Iniciar proyecto:

  * Instalar dependencias `mix deps.get`
  * Instalar dependencias Node.js `npm install` dentro del directorio `assets`
  * Levantar proyecto `sh run_okteto.sh`

## Secret Key Base (Opcional)
  Este paso se realiza cuando se quiere cambiar el secret key que ya existe por defecto

### Generar key dentro de okteto usando mix

```bash
iex> mix phx.gen.secret
```

### Generar key usando shell

  * Linux: `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1`
  * Mac: `cat /dev/urandom | env LC_CTYPE=C tr -dc a-zA-Z0-9 | head -c 64; echo`

### Actualizar key en la config

  * Reemplazar en el fichero `config/config.exs` el valor de la variable `secret_key_base` por el generado arriba
  * Reemplazar en el fichero `bitbucket-pipelines.yml` el valor de la variable `SECRETKEYBASE` por el generado arriba

## Configuración local:

  * Agregar el mismo host configurado del `run_okteto.sh` en `C:\Windows\System32\drivers\etc\hosts` para windows y `/etc/hosts` para linux
  * Agregar entradas nginx local:
  * Ejemplo cargando varias app phoenix:

```bash
http {
  map $http_upgrade $connection_upgrade {
      default upgrade;
      '' close;
  }

  server {
    listen 443 ssl;
    server_name _;

    location ~ ^/microfront(.*){
      proxy_pass https://canary-backofficedigital.totalcheck.cl/microfront$1$is_args$args;
    }

    location ~ ^/skeleton(.*)$ {
      set $port 3000;
      try_files $1 @apps;
    }

    location ~ ^/solicitudservicios(.*)$ {
      set $port 3001;
      try_files $1 @apps;
    }
    
    location ~ ^/([0-9a-z]+)(.*) {
       set $port 3000;
       try_files $1$2 @apps;
    }
                                        
    location @apps {
      client_max_body_size 10M;
      set $proxy_uri $uri$is_args$args;
      proxy_pass http://127.0.0.1:$port$proxy_uri;
      proxy_set_header host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_http_version 1.1;
      proxy_read_timeout 3600s;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection $connection_upgrade;
      proxy_set_header Origin ''; #alternativa check_origin: false
    }
  }
}
```

  * Levantar aplicación en el navegador: https://canaryy-backofficedigital.totalcheck.cl

## Documentación recomendada
### Phoenix

  * Sitio Oficial: [https://www.phoenixframework.org/](https://www.phoenixframework.org/)
  * Guías: [https://hexdocs.pm/phoenix/overview.html](https://hexdocs.pm/phoenix/overview.html)
  * Documentación: [https://hexdocs.pm/phoenix](https://hexdocs.pm/phoenix)
  * Forum: [https://elixirforum.com/c/phoenix-forum](https://elixirforum.com/c/phoenix-forum)
  * Código: [https://github.com/phoenixframework/phoenix](https://github.com/phoenixframework/phoenix)
  * Libro: [https://runebook.dev/en/docs/phoenix/-index-](https://runebook.dev/en/docs/phoenix/-index-)

### Surface

  * Sitio Oficial: [https://surface-ui.org/](https://surface-ui.org/)
  * Github: [https://github.com/surface-ui/surface](https://github.com/surface-ui/surface)
  * Similar a storybook para Surface: [https://github.com/surface-ui/surface_catalogue](https://github.com/surface-ui/surface_catalogue)

### LiveView

  * Tutorial Básico: [https://pragmaticstudio.com/tutorials](https://pragmaticstudio.com/tutorials)
  * Documentación: [https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html](https://hexdocs.pm/phoenix_live_view/Phoenix.LiveView.html)

### Tailwind

  * Componentes: [https://tailwindui.com/](https://tailwindui.com/)
  * Documentación: [https://tailwindcss.com/docs](https://tailwindcss.com/docs)

## Alpine.js
  * Documentación: [https://github.com/alpinejs/alpine](https://github.com/alpinejs/alpine)
  * Framework: [https://alpinejs.dev/](https://alpinejs.dev/)
