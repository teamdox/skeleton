defmodule Skeleton.MixProject do
  use Mix.Project

  def project do
    [
      app: :skeleton,
      version: "0.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers() ++ [:surface],
      build_embedded: isembedded(Mix.env()),
      start_permanent: isembedded(Mix.env()),
      aliases: aliases(),
      deps: deps(),
      releases: releases()
    ]
  end

  def releases() do
    [
      skeleton0: [
        start_distribution_during_config: true,
        include_executables_for: [:unix],
        cookie: "node",
        version: "0.1.0"
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Skeleton.Application, []},
      extra_applications: [:logger, :runtime_tools, :os_mon, :httpoison]
    ]
  end

  # Specifies build_embedded and start_permanent parameter
  defp isembedded(parameter), do: Enum.member?([:prod, :qa], parameter)

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.6.2"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_dashboard, "~> 0.5.3"},
      {:phoenix_components, git: "https://bitbucket.org/teamdox/phoenix_components"},
      {:floki, ">= 0.30.0"},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:cachex, "~> 3.3"},
      {:flex_logger, "~> 0.2.1"},
      {:httpoison, "~> 1.6.2"},
      {:etcdc, github: "haroldvera/etcdc"},
      {:albertocommon, git: "https://bitbucket.org/teamdox/albertocommon"},
      {:monad, git: "https://github.com/haroldvera/monad", branch: "develop"},
      {:hackney, "~> 1.17", override: true},
      {:number, "~> 1.0.1"},
      {:timex, "~> 3.7.6"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "cmd npm install --prefix assets"]
    ]
  end
end
