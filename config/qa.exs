use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with webpack to recompile .js and .css sources.

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
#
config :logger,
  backends: [{FlexLogger, :logger_name}]

config :logger, :logger_name,
  logger: :console,
  # this is the loggers default level
  default_level: :off,

  # override default levels
  level_config: [application: :skeleton, level: :info],
  # backend specific configuration
  format: "$time $metadata[$level] $levelpad$message\n",
  metadata: [:request_id]

config :logger,
  handle_otp_reports: false,
  handle_sasl_reports: false

config :sasl, sasl_error_logger: false
config :sasl, errlog_type: :error

config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

config :etcdc,
  etcd_host: "etcd-service",
  etcd_client_port: 4001
