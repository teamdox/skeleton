# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
import Config

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :skeleton, SkeletonWeb.Endpoint,
  http: [
    port: String.to_integer(System.get_env("PORT") || "4000")
  ],
  secret_key_base: secret_key_base,
  server: true,
  cache_static_manifest: "priv/static/cache_manifest.json",
  static_url: [path: "/skeleton/static"]

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :skeleton, MessonWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
#
config :logger,
  backends: [{FlexLogger, :logger_name}]

config :logger, :logger_name,
  logger: :console,
  # this is the loggers default level
  default_level: :off,

  # override default levels
  level_config: [application: :skeleton, level: :info],
  # backend specific configuration
  format: "$time $metadata[$level] $levelpad$message\n",
  metadata: [:request_id]

config :logger,
  handle_otp_reports: false,
  handle_sasl_reports: false

config :sasl, sasl_error_logger: false
config :sasl, errlog_type: :error

config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

config :etcdc,
  etcd_host: "etcd-service",
  etcd_client_port: 4001
