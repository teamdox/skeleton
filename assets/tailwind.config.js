const defaultTheme = require('tailwindcss/defaultTheme')
const plugin = require('tailwindcss/plugin')
module.exports = {
	purge: [],
	theme: {
			extend: {
			top :{
				'full': '100%'
			},
			colors: {
				primary: {
					'100': '#005FDDCC', //opacidad 0.8
					'200': '#005FDD', //color principal
					'300': '#004393', //color principal hover
					'400': '#F2F2F2', //header accordion
					'800': '#3C3C3C', //text acciones documento
					'900': '#4B5563' //color texto
				},
				notification: {
					'error': '#ff3636',
					'success': '#00BA83',
					'warn': '#FFA136',
					'info': '#369AFF'
				},
			},
			fontFamily: {
				sans: ['Inter var', ...defaultTheme.fontFamily.sans],
			},
			width: {
			  '11': '2.75rem'
			}}
	},
	variants: {
		backgroundColor: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
		textColor: ['responsive', 'hover', 'focus', 'group-hover'],
		opacity: ['responsive', 'hover', 'focus', 'active', 'group-hover'],
		display: ['responsive', 'hover', 'focus', 'group-hover'],
		width: ['responsive', 'hover', 'focus']
		//borderColor: ['hover', 'focus', 'group-hover'],
		//borderWidth: ['hover', 'focus', 'group-hover']
	},
	plugins: [
		plugin(function({ addUtilities }) {
		      const newUtilities = {
			'.required:before': {
			  color: '#FF3636 !important',
			  content: "'* '"
			}
		      }
		      addUtilities(newUtilities, ['responsive', 'hover'])
		    }),
	/*	require('@tailwindcss/ui')(
			{
				layout: 'sidebar'
			}
		),*/
		require('@tailwindcss/forms'),
		require('@tailwindcss/typography'),
		require('@tailwindcss/aspect-ratio'),
	]
}
